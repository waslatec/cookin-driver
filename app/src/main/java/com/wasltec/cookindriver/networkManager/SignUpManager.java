package com.wasltec.cookindriver.networkManager;

import android.app.Activity;
import android.app.ProgressDialog;
import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.wasltec.cookindriver.helpers.ResultHandler;
import com.wasltec.cookindriver.utils.Urls;

import org.json.JSONObject;

import amitsoftware.sweetalertdialogs.SweetAlertDialogProgress;

/**
 * Created by amr heider on 5/8/2017.
 */

public class SignUpManager {

    Activity context;
    ProgressDialog _ProgressDialog;
    private SignUpManager.MyCustomObjectListener listener;
    //    public static int log=0;
    public SignUpManager(Activity context) {
        this.context = context;
    }

    public void setCustomObjectListener(SignUpManager.MyCustomObjectListener listener) {
        this.listener = listener;
    }

    public interface MyCustomObjectListener {

        public void onObjectReady(String title);
        public void onFailed(String title);


    }


    public void signUp()

    {
        final SweetAlertDialogProgress sweetAlertDialogProgress = new SweetAlertDialogProgress(context);
        sweetAlertDialogProgress.show();
        AndroidNetworking.post(Urls.LOGIN_URL)
                .setTag("test")
                .setContentType("application/x-www-form-urlencoded")
                .setPriority(Priority.IMMEDIATE)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (listener != null){
                            if ( new ResultHandler().validateHandlerResult(context,response))

                                listener.onObjectReady(response.toString());
                        }
                        sweetAlertDialogProgress.dismiss();
                        // do anything with response
                        Log.d("apidata", response.toString());
                    }
                    @Override
                    public void onError(ANError error) {
                        if (listener != null){
                            listener.onFailed(error.getErrorBody().toString());
                        }
                        sweetAlertDialogProgress.dismiss();
                        // handle error
                        Log.d("apidata", error.getErrorBody().toString());
                    }
                });
    }



}
