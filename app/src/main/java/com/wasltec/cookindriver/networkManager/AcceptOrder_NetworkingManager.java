package com.wasltec.cookindriver.networkManager;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.wasltec.cookindriver.activities.MainActivity;
import com.wasltec.cookindriver.helpers.ResultHandler;
import com.wasltec.cookindriver.utils.LoginSharedPreferences;
import com.wasltec.cookindriver.utils.Urls;

import org.json.JSONException;
import org.json.JSONObject;

import amitsoftware.sweetalertdialogs.SweetAlertDialog;
import amitsoftware.sweetalertdialogs.SweetAlertDialogFailed;
import amitsoftware.sweetalertdialogs.SweetAlertDialogProgress;

public class AcceptOrder_NetworkingManager {
    Activity context;
    SweetAlertDialogProgress sweetalert;
    SweetAlertDialog sweetalert2;
    SweetAlertDialogFailed sweetalert3;
    private AcceptOrder_NetworkingManager.MyCustomObjectListener listener;

    public AcceptOrder_NetworkingManager(Activity context) {

        this.context = context;

        //sweetalert = new SweetAlertDialogProgress(context, SweetAlertDialogProgress.PROGRESS_TYPE);
        //sweetalert.setCancelable(false);
       // sweetalert.show();

    }
    public void setCustomObjectListener(AcceptOrder_NetworkingManager.MyCustomObjectListener listener) {
        this.listener = listener;
    }

    public interface MyCustomObjectListener {

        public void onObjectReady(String title);
        public void onFailed(String title);

    }
    public void Accept_Order(String orderID) {

        final SweetAlertDialogProgress sweetAlertDialogProgress = new SweetAlertDialogProgress(context);
        sweetAlertDialogProgress.show();

        Log.d("Authorization","Bearer "+ new LoginSharedPreferences(context).getAccessToken());
        Log.d("orderId",orderID);
        AndroidNetworking.post(Urls.ACCEPTORDER)
                . addBodyParameter("orderId", orderID)
                .addHeaders("Authorization","Bearer "+ new LoginSharedPreferences(context).getAccessToken())
                .setContentType("application/x-www-form-urlencoded")
                .setTag("AcceptOrder")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (listener != null){
                            if ( new ResultHandler().validateHandlerResult(context,response)){

                                listener.onObjectReady(response.toString());

                            }
                        }
                        sweetAlertDialogProgress.dismiss();
                        // do anything with response
                        Log.d("AcceptOrder", response.toString());

                    }

                    @Override
                    public void onError(ANError error) {
                        if (listener != null){
                            listener.onFailed(error.getErrorBody().toString());
                        }
                        sweetAlertDialogProgress.dismiss();

                        // handle error
                        Log.d("AcceptOrder", error.getErrorBody().toString());

                    }
                });

    }
}