package com.wasltec.cookindriver.networkManager;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.wasltec.cookindriver.activities.MainActivity;
import com.wasltec.cookindriver.activities.SignIn;
import com.wasltec.cookindriver.utils.LoginSharedPreferences;
import com.wasltec.cookindriver.utils.Urls;

import org.json.JSONObject;

import amitsoftware.sweetalertdialogs.SweetAlertDialog;
import amitsoftware.sweetalertdialogs.SweetAlertDialogFailed;
import amitsoftware.sweetalertdialogs.SweetAlertDialogProgress;

public class RegisterGCM_NetworkingManager {
    Activity context;
    SweetAlertDialogProgress sweetalert;
    SweetAlertDialog sweetalert2;
    SweetAlertDialogFailed sweetalert3;
    public RegisterGCM_NetworkingManager(Activity context) {

        this.context = context;

        //sweetalert = new SweetAlertDialogProgress(context, SweetAlertDialogProgress.PROGRESS_TYPE);
        //sweetalert.setCancelable(false);
       // sweetalert.show();

    }

    public void registerGCM(String deviceToken) {
        JSONObject jsonParams = null;
        try {
            jsonParams=new JSONObject();
            jsonParams.put("MobileTypeId", 0);
            jsonParams.put("PushKey", deviceToken);
            jsonParams.put("EMEI", deviceToken);
            jsonParams.put("IsPushEnabled", true);
            Log.d("GCMRegister",jsonParams.toString());
        } catch (Exception e) {
            Log.d("GCMRegister",e.toString());
        }

        Log.d("GCMRegisterdeviceToken",deviceToken);

        AndroidNetworking.post(Urls.ADDDEVICE)
                . addBodyParameter("MobileTypeId", "1")
                .addBodyParameter("PushKey", deviceToken)
                . addBodyParameter("EMEI", deviceToken)
                . addBodyParameter("IsPushEnabled", "true")
                .addHeaders("Authorization","Bearer "+ new LoginSharedPreferences(context).getAccessToken())
                .setContentType("application/x-www-form-urlencoded")
                .setTag("GCMRegister")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("GCMRegister",response.toString());
                        Intent intent = new Intent(context, MainActivity.class);
                        context.startActivity(intent);
                        context.finish();
//                            sweetalert.dismiss();
//                            sweetalert2=new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE);
//                            sweetalert2
//                                    .setTitleText(context.getResources().getString(R.string.Success))
//                                    .setContentText(context.getResources().getString(R.string.passwordchanged))
//                                    .setConfirmText(context.getResources().getString(R.string.ok))
//                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
//                                        @Override
//                                        public void onClick(SweetAlertDialog sDialog) {
//                                            sweetalert2.dismiss();
//                                            ActivitySwping.goTOAnotherActivityAndFinish(context, Activity_Home.class);
//                                        }
//                                    });
//                            sweetalert2.setCancelable(false);
//                            sweetalert2.show();

                    }

                    @Override
                    public void onError(ANError ANError) {
                        Log.d("GCMRegister",ANError.getErrorCode()+" - "+ANError.getErrorDetail().toString());

                        //Log.d("Login response",ANError.getErrorBody().toString()+" - " +ANError.getData().toString()+" - "+ANError.getErrorCode()+" - "+ANError.getErrorDetail().toString());
                        if(ANError.getErrorCode()==404||ANError.getErrorCode()==500||ANError.getErrorCode()==401||ANError.getErrorCode()==0)
                        {
                            //sweetalert.dismiss();
//                            sweetalert3=new SweetAlertDialogFailed(context, SweetAlertDialogFailed.WARNING_TYPE);
//                            //  sweetalert.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
//                            sweetalert3
//                                    .setTitleText(context.getResources().getString(R.string.Error))
//                                    .setContentText(context.getResources().getString(R.string.no_internet))
//                                    .setConfirmText(context.getResources().getString(R.string.ok))
//
//
//                                    .setConfirmClickListener(new SweetAlertDialogFailed.OnSweetClickListener() {
//                                        @Override
//                                        public void onClick(SweetAlertDialogFailed sDialog) {
//                                            sDialog.dismissWithAnimation();
//                                            // ActivitySwping.goTOAnotherActivityAndFinish(Activity_Home.this, EpicLoginActivity.class);
//                                        }
//                                    });
//                            sweetalert3.setCancelable(false);
//                            sweetalert3.show();

                        }
                        else if(ANError.getErrorCode()==401)
                        {
                           // sweetalert.dismiss();
//                            sweetalert3=new SweetAlertDialogFailed(context, SweetAlertDialogFailed.WARNING_TYPE);
//                            //  sweetalert.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
//                            sweetalert3
//                                    .setTitleText(context.getResources().getString(R.string.Error))
//                                    .setContentText(context.getResources().getString(R.string.no_internet))
//                                    .setConfirmText(context.getResources().getString(R.string.ok))
//
//
//                                    .setConfirmClickListener(new SweetAlertDialogFailed.OnSweetClickListener() {
//                                        @Override
//                                        public void onClick(SweetAlertDialogFailed sDialog) {
//                                            sDialog.dismissWithAnimation();
//                                            // ActivitySwping.goTOAnotherActivityAndFinish(Activity_Home.this, EpicLoginActivity.class);
//                                        }
//                                    });
//                            sweetalert3.setCancelable(false);
//                            sweetalert3.show();

                        }


                    }
                });

    }
}