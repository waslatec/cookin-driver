package com.wasltec.cookindriver.networkManager;

import android.app.Activity;
import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.wasltec.cookindriver.helpers.ResultHandler;
import com.wasltec.cookindriver.utils.LoginSharedPreferences;
import com.wasltec.cookindriver.utils.Urls;

import org.json.JSONException;
import org.json.JSONObject;

import amitsoftware.sweetalertdialogs.SweetAlertDialog;
import amitsoftware.sweetalertdialogs.SweetAlertDialogFailed;
import amitsoftware.sweetalertdialogs.SweetAlertDialogProgress;

public class GetOrder_NetworkingManager {
    Activity context;
    SweetAlertDialogProgress sweetalert;
    SweetAlertDialog sweetalert2;
    SweetAlertDialogFailed sweetalert3;
    private GetOrder_NetworkingManager.MyCustomObjectListener listener;

    public GetOrder_NetworkingManager(Activity context) {

        this.context = context;

        //sweetalert = new SweetAlertDialogProgress(context, SweetAlertDialogProgress.PROGRESS_TYPE);
        //sweetalert.setCancelable(false);
       // sweetalert.show();

    }
    public void setCustomObjectListener(GetOrder_NetworkingManager.MyCustomObjectListener listener) {
        this.listener = listener;
    }

    public interface MyCustomObjectListener {

        public void onObjectReady(String title);
        public void onFailed(String title);

    }
    public void Get_Order() {

        final SweetAlertDialogProgress sweetAlertDialogProgress = new SweetAlertDialogProgress(context);
        sweetAlertDialogProgress.show();

        Log.d("Authorization","Bearer "+ new LoginSharedPreferences(context).getAccessToken());
        AndroidNetworking.post(Urls.GETCURRENTORDER)
                .addHeaders("Authorization","Bearer "+ new LoginSharedPreferences(context).getAccessToken())
                .setContentType("application/x-www-form-urlencoded")
                .setTag("Get_Order")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("Get_Order", response.toString());

                        if (listener != null){
                            if ( new ResultHandler().validateHandlerResult(context,response)){

                                try {
                                    listener.onObjectReady(response.getJSONObject("Data").toString());
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        }
                        sweetAlertDialogProgress.dismiss();
                        // do anything with response

                    }

                    @Override
                    public void onError(ANError error) {
                        Log.d("Get_Order", error.getErrorBody().toString());

                        if (listener != null){
                            listener.onFailed(error.getErrorBody().toString());
                        }
                        sweetAlertDialogProgress.dismiss();

                        // handle error

                    }
                });

    }
}