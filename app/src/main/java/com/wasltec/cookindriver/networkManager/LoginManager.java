package com.wasltec.cookindriver.networkManager;


import android.app.Activity;
import android.app.ProgressDialog;
import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.wasltec.cookindriver.helpers.ResultHandler;
import com.wasltec.cookindriver.utils.LoginSharedPreferences;
import com.wasltec.cookindriver.utils.Urls;

import org.json.JSONException;
import org.json.JSONObject;

import amitsoftware.sweetalertdialogs.SweetAlertDialogProgress;

/**
 * Created by amr heider on 5/8/2017.
 */

public class LoginManager {

    Activity context;
    LoginSharedPreferences loginSharedPreferences;
    ProgressDialog _ProgressDialog;
    private MyCustomObjectListener listener;
    //    public static int log=0;
    public LoginManager(Activity context) {
        this.context = context;
    }

    public void setCustomObjectListener(MyCustomObjectListener listener) {
        this.listener = listener;
    }

    public interface MyCustomObjectListener {

        public void onObjectReady(String title);
        public void onFailed(String title);

    }


    public void logIn(String MobileNumber, String Password)

    {
        final SweetAlertDialogProgress sweetAlertDialogProgress = new SweetAlertDialogProgress(context);
        sweetAlertDialogProgress.show();
        AndroidNetworking.post(Urls.LOGIN_URL)
                .addBodyParameter("MobileNumber", "2"+MobileNumber)
                .addBodyParameter("Password", Password)
                .setTag("test")
                .setContentType("application/x-www-form-urlencoded")
                .setPriority(Priority.IMMEDIATE)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (listener != null){
                            if ( new ResultHandler().validateHandlerResult(context,response)){
                                try {
                                    loginSharedPreferences = new LoginSharedPreferences(context);
                                    loginSharedPreferences.setAccessToken(response.getString("Token"));
                                    loginSharedPreferences.setPhoneNumber(response.getJSONObject("client").getString("MobileNumber"));
                                    loginSharedPreferences.setPassword(response.getJSONObject("client").getString("password"));
                                    Log.v("TOKEN",loginSharedPreferences.getAccessToken());
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                listener.onObjectReady(response.toString());

                            }
                        }
                        sweetAlertDialogProgress.dismiss();
                        // do anything with response
                        Log.d("Login", response.toString());
                    }
                    @Override
                    public void onError(ANError error) {
                        if (listener != null){
                            listener.onFailed(error.getErrorBody().toString());
                        }
                        sweetAlertDialogProgress.dismiss();

                        // handle error
                        Log.d("Login", error.getErrorBody().toString());
                    }
                });
    }

}
