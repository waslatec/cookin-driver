package com.wasltec.cookindriver.models;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Created by yo7ia on 5/10/17.
 */

public class NewOrderModel implements Serializable {
    public enum OrderStatus
    {
        Requested,
        DriverApproved,
        DriverTakeItems,
        Delivered,
        Finished,
        Canceled,
        Rejected,
        Transfered,
    }
        public Integer OrderId;


        public OrderLocation CheifLocation;
        public OrderLocation ClientLocation;
        public OrderStatus orderStatus;
        public NewOrderModel(JSONObject map){
            try {
                this.OrderId = map.getInt("OrderId");

                Integer state = map.has("OrderState")?map.getInt("OrderState"):0;
                switch (state)
                {
                    case 0:
                        this.orderStatus = OrderStatus.Requested;
                        break;
                    case 1:
                        this.orderStatus = OrderStatus.DriverApproved;
                        break;
                    case 2:
                        this.orderStatus = OrderStatus.DriverTakeItems;
                        break;
                    case 3:
                        this.orderStatus = OrderStatus.Delivered;
                        break;
                    case 4:
                        this.orderStatus = OrderStatus.Finished;
                        break;
                    case 5:
                        this.orderStatus = OrderStatus.Canceled;
                        break;
                    case 6:
                        this.orderStatus = OrderStatus.Rejected;
                        break;
                    case 7:
                        this.orderStatus = OrderStatus.Transfered;
                        break;
                    default:
                        this.orderStatus = OrderStatus.Requested;
                        break;
                }
                this.ClientLocation = new OrderLocation(map.getJSONObject("ClientLocation"));
                this.CheifLocation = new OrderLocation(map.getJSONObject("CheifLocation"));

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }


        public class OrderLocation implements Serializable {
            public Double Lng,Lat;
            public OrderLocation(JSONObject data) {
                try {
                    this.Lng = data.getDouble("Lng");
                    this.Lat = data.getDouble("lat");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }
    }


