package com.wasltec.cookindriver.notificationHelper;

import android.util.Log;


import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by Amr Heidar on 18-Dec-16.
 */
public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    // [START refresh_token]
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(refreshedToken);
    }

    private void sendRegistrationToServer(String token) {

//        if (PrefManager.isLoggedIn(getApplicationContext())==1) {
//            User user = PrefManager.getUserParsed(getApplicationContext());
//            RegisterGCM_NetworkingManager gcmManager = new RegisterGCM_NetworkingManager(getApplicationContext());
//            gcmManager.registerGCM(user.id, token);
//            Log.d("token", token);
//        }

    }
}
