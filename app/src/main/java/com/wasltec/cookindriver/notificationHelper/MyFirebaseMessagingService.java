package com.wasltec.cookindriver.notificationHelper;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;


import com.wasltec.cookindriver.R;
import com.wasltec.cookindriver.activities.MainActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.wasltec.cookindriver.models.NewOrderModel;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Amr Heidar on 18-Dec-16.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {


    private static final String TAG = "MyFirebaseMsgService";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {


        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
            Log.d("TestNotification"+remoteMessage.getData().get("title"),remoteMessage.getData().get("message"));
//            sendNotification(remoteMessage.getData().get("title"),remoteMessage.getData().get("message"));
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
            try {
                JSONObject data = new JSONObject(remoteMessage.getNotification().getBody());
                if(data.has("OrderId"))
                {
                    NewOrderModel newOrderModel = new NewOrderModel(data);
                    sendNotification("New Order",newOrderModel);
                    Intent intent = new Intent();
                    intent.setAction("NewOrder");

                    intent.putExtra("NewOrder", newOrderModel);
                    getApplicationContext().sendBroadcast(intent);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


    }


    private void sendNotification(String title,NewOrderModel newOrderModel) {
        Intent intent = new Intent();
        intent.setAction("NewOrder");
        intent.putExtra("NewOrder", newOrderModel);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText("Order Id :"+newOrderModel.OrderId)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }
}
