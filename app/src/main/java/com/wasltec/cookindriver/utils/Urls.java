package com.wasltec.cookindriver.utils;

/**
 * Created by Amit11 on 11/17/2016.
 */
public class Urls {
    public static final String SERVERURL = "http://api.coockin.wasltec.com/";
    public static final String LOGIN_URL = SERVERURL+"api/Driver/Login";
    public static final String ADDDEVICE =SERVERURL+ "api/Driver/AddDevice";
    public static final String ACCEPTORDER =SERVERURL+ "api/Driver/AcceptOrder";
    public static final String GETCURRENTORDER =SERVERURL+ "api/Driver/GetOrder";
    public static final String TAKEORDER =SERVERURL+ "api/Driver/TakeOrder";
    public static final String DELIVEREDORDER =SERVERURL+ "api/Driver/DeliveredOrder";


}
