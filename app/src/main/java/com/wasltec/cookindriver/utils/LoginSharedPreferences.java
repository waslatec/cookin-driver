package com.wasltec.cookindriver.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by amr heider on 4/6/2017.
 */

public class LoginSharedPreferences {

    private Context context;
    private String phoneNumber, password, accessToken;
    SharedPreferences.Editor editor;
    SharedPreferences settings;


    public LoginSharedPreferences(Context context) {
        this.context = context;
        settings = context.getSharedPreferences("login", 0);
        editor = settings.edit();
    }



    public String getPhoneNumber() {
        settings = context.getSharedPreferences("login", 0);
        return settings.getString("phoneNumber","");
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        settings = context.getSharedPreferences("login", 0);
        editor = settings.edit();
        editor.putString("phoneNumber", this.phoneNumber);
        editor.commit();
    }

    public String getPassword() {
        settings = context.getSharedPreferences("login", 0);
        return settings.getString("password","");
    }

    public void setPassword(String password) {
        this.password = password;
        settings = context.getSharedPreferences("login", 0);
        editor = settings.edit();
        editor.putString("password", this.password);
        editor.commit();
    }

    public String getAccessToken() {
        settings = context.getSharedPreferences("login", 0);
        return settings.getString("accessToken", "");
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
        settings = context.getSharedPreferences("login", 0);
        editor = settings.edit();
        editor.putString("accessToken", accessToken);
        editor.commit();

    }


    public void removeLogin(Context context) {
        settings = context.getSharedPreferences("login", 0);
        editor = settings.edit();
        editor.remove("accessToken");
        editor.remove("phoneNumber");
        editor.remove("password");
        editor.commit();
    }
}
