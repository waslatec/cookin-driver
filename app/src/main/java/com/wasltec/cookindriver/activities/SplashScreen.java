package com.wasltec.cookindriver.activities;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.google.firebase.iid.FirebaseInstanceId;
import com.wasltec.cookindriver.R;
import com.wasltec.cookindriver.networkManager.LoginManager;
import com.wasltec.cookindriver.networkManager.RegisterGCM_NetworkingManager;
import com.wasltec.cookindriver.utils.LoginSharedPreferences;


public class SplashScreen extends AppCompatActivity {
    LoginSharedPreferences loginSharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        loginSharedPreferences = new LoginSharedPreferences(this);

        getSupportActionBar().hide();
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE  | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        waitting();
    }

    private void waitting(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (loginSharedPreferences.getAccessToken()==null||loginSharedPreferences.getAccessToken()=="")
                {

                    toHome();
                }
                else
                {
                    LoginManager loginManager = new LoginManager(SplashScreen.this);
                    loginManager.setCustomObjectListener(new LoginManager.MyCustomObjectListener() {
                        @Override
                        public void onObjectReady(String title) {
                            Log.e("onObjectReadyLogin",title);
                            String refreshedToken = FirebaseInstanceId.getInstance().getToken();
                            RegisterGCM_NetworkingManager gcmManager = new RegisterGCM_NetworkingManager(SplashScreen.this);
                            gcmManager.registerGCM(refreshedToken);
                           }

                        @Override
                        public void onFailed(String title) {
                            Log.e("onFailedLogin",title);
                        }
                    });

                    loginManager.logIn(loginSharedPreferences.getPhoneNumber(),loginSharedPreferences.getPassword());

                }

            }
        }, 3000);
    }

    private void toHome()
    {
        Intent intent = new Intent(SplashScreen.this,IntroApp.class);
        startActivity(intent);
        finish();
    }

}
