package com.wasltec.cookindriver.activities;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.wasltec.cookindriver.R;
import com.wasltec.cookindriver.activities.SignUp;
import com.wasltec.cookindriver.networkManager.LoginManager;
import com.wasltec.cookindriver.networkManager.RegisterGCM_NetworkingManager;

import amitsoftware.sweetalertdialogs.SweetAlertDialogFailed;


public class SignIn extends AppCompatActivity {

    EditText phone, password;
    Button sign;
    TextView have;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        getSupportActionBar().hide();
        initialize();
        clicks();

    }

    private void initialize()
    {
        phone = (EditText)findViewById(R.id.phone);
        password = (EditText)findViewById(R.id.password);
        sign = (Button)findViewById(R.id.signin) ;
        have = (TextView)findViewById(R.id.neww);

    }

    private void clicks()
    {
        have.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(SignIn.this,SignUp.class);
                startActivity(intent);
            }
        });

        sign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!Validate())
                    return;
                LoginManager loginManager = new LoginManager(SignIn.this);
                loginManager.setCustomObjectListener(new LoginManager.MyCustomObjectListener() {
                    @Override
                    public void onObjectReady(String title) {
                        Log.e("onObjectReadyLogin",title);
                        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
                        RegisterGCM_NetworkingManager gcmManager = new RegisterGCM_NetworkingManager(SignIn.this);
                        gcmManager.registerGCM(refreshedToken);
                    }

                    @Override
                    public void onFailed(String title) {
                        Log.e("onFailedLogin",title);
                    }
                });
                loginManager.logIn(phone.getText().toString(),password.getText().toString());


            }
        });


    }
    private Boolean Validate()
    {

        if (phone.getText().toString().trim().length() == 0 || password.getText().toString().trim().length() == 0 )
        {
            SweetAlertDialogFailed ahmed = new SweetAlertDialogFailed(this);
            ahmed.setTitleText("Note");
            ahmed.setContentText("Fill All Fields");
            ahmed.show();
            return false;
        }
        return true;
    }

}
