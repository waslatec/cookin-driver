package com.wasltec.cookindriver.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.wasltec.cookindriver.R;


public class IntroApp extends AppCompatActivity {

    Button sign;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro_app);

        getSupportActionBar().hide();
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE  | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        initialize();
        clicks();
    }

    private void initialize()
    {
        sign = (Button)findViewById(R.id.signInButton);
    }

    private void clicks()
    {
        sign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(IntroApp.this,SignIn.class);
                startActivity(intent);

            }
        });
    }
}
