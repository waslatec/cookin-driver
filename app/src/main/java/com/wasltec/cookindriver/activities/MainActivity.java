package com.wasltec.cookindriver.activities;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;


import com.google.android.gms.maps.CameraUpdate;
import com.wasltec.cookindriver.R;
import com.wasltec.cookindriver.dialogs.newOrder;
import com.wasltec.cookindriver.helpers.GMapV2Direction;
import com.wasltec.cookindriver.helpers.GPSTracker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.wasltec.cookindriver.models.NewOrderModel;
import com.wasltec.cookindriver.networkManager.AcceptOrder_NetworkingManager;
import com.wasltec.cookindriver.networkManager.DeliveredOrder_NetworkingManager;
import com.wasltec.cookindriver.networkManager.GetOrder_NetworkingManager;
import com.wasltec.cookindriver.networkManager.LoginManager;
import com.wasltec.cookindriver.networkManager.TakeOrder_NetworkingManager;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;

import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import amitsoftware.sweetalertdialogs.SweetAlertDialog;
import amitsoftware.sweetalertdialogs.SweetAlertDialogFailed;

public class MainActivity extends FragmentActivity implements OnMapReadyCallback,GoogleMap.OnMarkerClickListener {

    SupportMapFragment mMapFragment;
    FragmentTransaction fragmentTransaction;
    GoogleMap googleMap;
    GPSTracker gpsTracker;
    double myLat, myLng;
    PolylineOptions polylineOptions;
    GMapV2Direction gMapV2Direction;
    Document document;
    LatLng src, des;
    Button takeitems,deliveritems;
    NewOrderModel newOrderModel;
    Dialog dialog;
    newOrder.OnOrderAcceptClickListener onOrderAcceptClickListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mMapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mapp);
        mMapFragment.getMapAsync(this);
        initialization();
    }

    private void  GetCurrentOrder()
    {

//        if (newOrderModel == null)
//        {
            GetOrder_NetworkingManager getOrder_networkingManager = new GetOrder_NetworkingManager(this);
            getOrder_networkingManager.setCustomObjectListener(new GetOrder_NetworkingManager.MyCustomObjectListener() {
                @Override
                public void onObjectReady(String title) {

                    try {
                        newOrderModel = new NewOrderModel(new JSONObject(title));
                        switch (newOrderModel.orderStatus)
                        {
                            case Requested:
                                takeitems.setVisibility(View.INVISIBLE);
                                deliveritems.setVisibility(View.INVISIBLE);
                                newOrder order = new newOrder(MainActivity.this, newOrderModel, onOrderAcceptClickListener);
                                order.show();
                                break;
                            case DriverApproved:
                                takeitems.setVisibility(View.VISIBLE);
                                deliveritems.setVisibility(View.INVISIBLE);
                                StartTrackingOrder(newOrderModel,false);
                                break;
                            case DriverTakeItems:
                                takeitems.setVisibility(View.INVISIBLE);
                                deliveritems.setVisibility(View.VISIBLE);
                                StartTrackingOrder(newOrderModel,true);
                                break;
                            case Delivered:
                                takeitems.setVisibility(View.INVISIBLE);
                                deliveritems.setVisibility(View.VISIBLE);
                                break;
                            default:
                                break;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                @Override
                public void onFailed(String title) {

                }
            });
            getOrder_networkingManager.Get_Order();
//        }
    }
    private void initialization(){
        gpsTracker = new GPSTracker(this);
        takeitems = (Button) findViewById(R.id.takeItems);
        deliveritems = (Button) findViewById(R.id.DeliverItems);
        takeitems.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TakeItems();
            }
        });
        deliveritems.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DeliverItems();
            }
        });
        GetCurrentOrder();
        if(newOrderModel == null)
        {
            takeitems.setVisibility(View.GONE);
            deliveritems.setVisibility(View.GONE);
        }
        myLat = gpsTracker.getLatitude();
        myLng = gpsTracker.getLongitude();
        onOrderAcceptClickListener = new newOrder.OnOrderAcceptClickListener() {
            @Override
            public void setOnAcceptClick(final NewOrderModel orderModel) {

                AcceptOrder_NetworkingManager acceptOrder_networkingManager = new AcceptOrder_NetworkingManager(MainActivity.this);
                acceptOrder_networkingManager.setCustomObjectListener(new AcceptOrder_NetworkingManager.MyCustomObjectListener() {
                    @Override
                    public void onObjectReady(String title) {
                    Log.d("AcceptOrderSuccess_",title);
                        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(MainActivity.this, SweetAlertDialogFailed.SUCCESS_TYPE);
                        sweetAlertDialog.setTitle("Success");
                        sweetAlertDialog.setConfirmText("OK");

                        try {
                            sweetAlertDialog.setContentText(new JSONObject(title).getString("EnglishMessage"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        sweetAlertDialog.show();
                        takeitems.setVisibility(View.VISIBLE);
                        StartTrackingOrder(orderModel,true);
                    }

                    @Override
                    public void onFailed(String title) {
                        newOrderModel = null;
                        Log.d("AcceptOrder_onFailed",title);

                    }
                });
                acceptOrder_networkingManager.Accept_Order(String.valueOf(newOrderModel.OrderId));
            }
        };
    }
    BroadcastReceiver ResponseReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (newOrderModel == null)
            {
            newOrderModel = (NewOrderModel) intent.getSerializableExtra("NewOrder");
                switch (newOrderModel.orderStatus)
                {
                    case Requested:
                        takeitems.setVisibility(View.INVISIBLE);
                        deliveritems.setVisibility(View.INVISIBLE);
                        newOrder order = new newOrder(MainActivity.this, newOrderModel, onOrderAcceptClickListener);
                        order.show();
                        break;
                    case DriverApproved:
                        takeitems.setVisibility(View.VISIBLE);
                        deliveritems.setVisibility(View.INVISIBLE);
                        StartTrackingOrder(newOrderModel,false);
                        break;
                    case DriverTakeItems:
                        takeitems.setVisibility(View.INVISIBLE);
                        deliveritems.setVisibility(View.VISIBLE);
                        StartTrackingOrder(newOrderModel,true);
                        break;
                    case Delivered:
                        takeitems.setVisibility(View.INVISIBLE);
                        deliveritems.setVisibility(View.VISIBLE);
                        break;
                    default:
                        break;
                }
            }
            Log.d("ReceiverNewOrder",newOrderModel.OrderId+" - ");

        }
    };

    void TakeItems()
    {

        TakeOrder_NetworkingManager acceptOrder_networkingManager = new TakeOrder_NetworkingManager(MainActivity.this);
        acceptOrder_networkingManager.setCustomObjectListener(new TakeOrder_NetworkingManager.MyCustomObjectListener() {
            @Override
            public void onObjectReady(String title) {
                Log.d("AcceptOrderSuccess_",title);
                takeitems.setVisibility(View.INVISIBLE);
                deliveritems.setVisibility(View.VISIBLE);
                SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(MainActivity.this, SweetAlertDialogFailed.SUCCESS_TYPE);
                sweetAlertDialog.setTitle("Success");
                sweetAlertDialog.setConfirmText("OK");
                StartTrackingOrder(newOrderModel,false);

                try {
                    sweetAlertDialog.setContentText(new JSONObject(title).getString("EnglishMessage"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                sweetAlertDialog.show();

            }

            @Override
            public void onFailed(String title) {
//                newOrderModel = null;
                Log.d("AcceptOrder_onFailed",title);

            }
        });
        acceptOrder_networkingManager.Take_Order(String.valueOf(newOrderModel.OrderId));
    }

    void DeliverItems()
    {
        DeliveredOrder_NetworkingManager acceptOrder_networkingManager = new DeliveredOrder_NetworkingManager(MainActivity.this);
        acceptOrder_networkingManager.setCustomObjectListener(new DeliveredOrder_NetworkingManager.MyCustomObjectListener() {
            @Override
            public void onObjectReady(String title) {
                takeitems.setVisibility(View.GONE);
                deliveritems.setVisibility(View.GONE);
                Log.d("AcceptOrderSuccess_",title);
                SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(MainActivity.this, SweetAlertDialogFailed.SUCCESS_TYPE);
                sweetAlertDialog.setTitle("Success");
                sweetAlertDialog.setConfirmText("OK");

                try {
                    sweetAlertDialog.setContentText(new JSONObject(title).getString("EnglishMessage"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                sweetAlertDialog.show();
                googleMap.clear();
                googleMap.addMarker(new MarkerOptions().title("Me")
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE))
                        .position(src));
            }

            @Override
            public void onFailed(String title) {
//                newOrderModel = null;
                Log.d("AcceptOrder_onFailed",title);

            }
        });
        acceptOrder_networkingManager.Delivered_Order(String.valueOf(newOrderModel.OrderId));
    }

    void StartTrackingOrder(NewOrderModel newOrderModel,Boolean takeOrDeliver)
    {
//        takeitems.setVisibility(View.VISIBLE);
//        deliveritems.setVisibility(View.VISIBLE);

        LatLng latLngDes = takeOrDeliver ? new LatLng(newOrderModel.CheifLocation.Lat, newOrderModel.CheifLocation.Lng) : new LatLng(newOrderModel.ClientLocation.Lat, newOrderModel.ClientLocation.Lng);
        googleMap.clear();
        try {

            LatLng latLngSrc = new LatLng(myLat, myLng);
            googleMap.setOnMarkerClickListener(this);
            googleMap.addMarker(new MarkerOptions().title("Me")
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE))
                    .position(latLngSrc));

            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLngSrc, 17);
            googleMap.animateCamera(cameraUpdate);
            src = latLngSrc;

        } catch (Exception e){
            e.printStackTrace();
        }
        googleMap.addMarker(new MarkerOptions().position(latLngDes));
        des = latLngDes;
        new GetDirection().execute();
    }
    @Override
    protected void onResume() {
        super.onResume();
        RegisterReciver();
    }

    void RegisterReciver() {
        registerReceiver(this.ResponseReceiver, new IntentFilter("NewOrder"));
    }
    @Override
    public boolean onMarkerClick(Marker marker)
    {
        return false;
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {

        googleMap.clear();
        this.googleMap = googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
        try {

            LatLng latLngSrc = new LatLng(myLat, myLng);
            googleMap.setOnMarkerClickListener(this);
            googleMap.addMarker(new MarkerOptions().title("Me")
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE))
                    .position(latLngSrc));
            src = latLngSrc;

        } catch (Exception e){
            e.printStackTrace();
        }



    }


    private class GetDirection extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            gMapV2Direction = new GMapV2Direction();
            document = gMapV2Direction.getDocument(src, des,
                    GMapV2Direction.MODE_DRIVING);
            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {
            dialog.dismiss();
            try {
//                calculateTime(gMapV2Direction.getTimeValue(document));
//                calculateDistance(gMapV2Direction.getDistanceValue(document));
                ArrayList<LatLng> directionPoint = gMapV2Direction.getDirection(document);
                polylineOptions = new PolylineOptions().width(5).color(Color.RED);
                for (int i = 0; i < directionPoint.size(); i++)
                    polylineOptions.add(directionPoint.get(i));
                googleMap.addPolyline(polylineOptions);

                DOMSource domSource = new DOMSource(document);
                StringWriter writer = new StringWriter();
                StreamResult resultt = new StreamResult(writer);
                TransformerFactory tf = TransformerFactory.newInstance();
                Transformer transformer = tf.newTransformer();
                transformer.transform(domSource, resultt);
            } catch (Exception e){
                e.printStackTrace();
            }
        }

        @Override
        protected void onPreExecute() {
            dialog = new Dialog(MainActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.progress_dialog);
            dialog.setCancelable(false);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();
        }
    }


}

