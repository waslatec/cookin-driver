package com.wasltec.cookindriver.helpers;

import android.app.Activity;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import amitsoftware.sweetalertdialogs.SweetAlertDialogFailed;

/**
 * Created by amr heider on 4/9/2017.
 */

public class ResultHandler {

    public ResultHandler()
    {

    }

    public Boolean validateHandlerResult(Activity activity,JSONObject jsonObject)
    {

        if (jsonObject.has("Success"))
        {
            try {
                if (jsonObject.getBoolean("Success")== true)
                {
//                    SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(activity,SweetAlertDialog.SUCCESS_TYPE);
//                    sweetAlertDialog.setContentText("Success");
//                    sweetAlertDialog.setConfirmText("Continue");
//                    sweetAlertDialog.show();
                    return true;
                }
                else
                {
                    SweetAlertDialogFailed sweetAlertDialog = new SweetAlertDialogFailed(activity, SweetAlertDialogFailed.WARNING_TYPE);
                    sweetAlertDialog.setTitle("Error");
                    sweetAlertDialog.setConfirmText("Dismiss");

                    sweetAlertDialog.setContentText(jsonObject.getString("EnglishMessage"));
                    sweetAlertDialog.show();
                    return false;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        else
        {
            try {
                Toast.makeText(activity, jsonObject.getString("Message"), Toast.LENGTH_SHORT).show();
                return false;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
       return true;
    }
}
