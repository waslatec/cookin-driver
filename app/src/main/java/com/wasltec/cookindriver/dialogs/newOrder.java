package com.wasltec.cookindriver.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.wasltec.cookindriver.R;
import com.wasltec.cookindriver.models.NewOrderModel;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by raed on 02/05/2017.
 */

public class newOrder extends Dialog {

    public Activity c;
    public Dialog d;
    public ImageView img_accept, img_cancel;
    public LinearLayout lin_accept, lin_cancel;
    public TextView txt_accept, txt_cancel,OrderID;
    OnOrderAcceptClickListener listner;
    NewOrderModel orderModel ;
    public newOrder(Activity a, NewOrderModel orderModel, OnOrderAcceptClickListener listner) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
        this.orderModel = orderModel;
        this.listner = listner;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setBackgroundDrawable(new ColorDrawable(c.getResources().getColor(R.color.tran_blue)));
//        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_order);

        lin_accept = (LinearLayout) findViewById(R.id.lin_accept);
        lin_cancel = (LinearLayout) findViewById(R.id.lin_cancel);
        img_accept = (ImageView) findViewById(R.id.img_accept);
        img_cancel = (ImageView) findViewById(R.id.img_cancel);
        txt_accept = (TextView) findViewById(R.id.txt_accept);
        txt_cancel = (TextView) findViewById(R.id.txt_cancel);
        OrderID = (TextView) findViewById(R.id.textView);
        lin_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newOrder.this.dismiss();
            }
        });

        OrderID.setText("Order Id :"+orderModel.OrderId);

        lin_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newOrder.this.dismiss();
                listner.setOnAcceptClick(orderModel);
        }
          });


    }


    public interface OnOrderAcceptClickListener {
        void setOnAcceptClick(NewOrderModel newOrderModel);
    }

}
