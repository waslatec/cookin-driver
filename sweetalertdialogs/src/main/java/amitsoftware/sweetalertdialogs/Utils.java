package amitsoftware.sweetalertdialogs;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.telephony.TelephonyManager;
import android.view.Display;
import android.view.WindowManager;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by Android Developer on 2/24/2016.
 */public class Utils {
    public static String getTimeAgo(long time) {
         final List<Long> times = Arrays.asList(
                TimeUnit.DAYS.toMillis(365),
                TimeUnit.DAYS.toMillis(30),
                TimeUnit.DAYS.toMillis(1),
                TimeUnit.HOURS.toMillis(1),
                TimeUnit.MINUTES.toMillis(1),
                TimeUnit.SECONDS.toMillis(1) );
         final List<String> timesString = Arrays.asList("year","month","day","hour","minute","second");
          final int SECOND_MILLIS = 1000;
          final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
          final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
          final int DAY_MILLIS = 24 * HOUR_MILLIS;
          final int MONTH_MILLIS = 30 * HOUR_MILLIS;
        if (time < 1000000000000L) {
            // if timestamp given in seconds, convert to millis
            time *= 1000;
        }

        long now = new Date().getTime();
        if (time > now || time <= 0) {
            return "just now";
        }

        // TODO: localize
        final long diff = now - time;
        if (diff < MINUTE_MILLIS) {
            return "الأن";
        } else if (diff < 2 * MINUTE_MILLIS) {
            return "منذ دقيقة";
        } else if (diff < 50 * MINUTE_MILLIS) {
            return "منذ "+diff / MINUTE_MILLIS + "دقيقة";
        } else if (diff < 90 * MINUTE_MILLIS) {
            return "منذ ساعة";
        } else if (diff < 24 * HOUR_MILLIS) {
            if(diff/HOUR_MILLIS==1)
            return "منذ " + "ساعة";
            else if(diff/HOUR_MILLIS==2)
            return "منذ " + "ساعتين";
            else
            return "منذ "+diff / HOUR_MILLIS + "ساعات";
        } else if (diff < 48 * HOUR_MILLIS) {
            return "بالأمس";
        } else if (diff < 30 * DAY_MILLIS) {
            if(diff/DAY_MILLIS<10)
            return "منذ "+diff / DAY_MILLIS + " أيام";
            else
                return "منذ "+diff / DAY_MILLIS + " يوم";
        }
        else
        {
            if(diff/MONTH_MILLIS==1)
            return "منذ "+diff /MONTH_MILLIS  + " شهر ";
           else
                return "منذ "+diff /MONTH_MILLIS  + " أشهر ";
        }
    }
//    public static String getTimeAgo(long time) {
//        final List<Long> times = Arrays.asList(
//                TimeUnit.DAYS.toMillis(365),
//                TimeUnit.DAYS.toMillis(30),
//                TimeUnit.DAYS.toMillis(1),
//                TimeUnit.HOURS.toMillis(1),
//                TimeUnit.MINUTES.toMillis(1),
//                TimeUnit.SECONDS.toMillis(1) );
//        final List<String> timesString = Arrays.asList("year","month","day","hour","minute","second");
//        final int SECOND_MILLIS = 1000;
//        final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
//        final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
//        final int DAY_MILLIS = 24 * HOUR_MILLIS;
//        if (time < 1000000000000L) {
//            // if timestamp given in seconds, convert to millis
//            time *= 1000;
//        }
//
//        long now = new Date().getTime();
//        if (time > now || time <= 0) {
//            return "just now";
//        }
//
//        // TODO: localize
//        final long diff = now - time;
//        if (diff < MINUTE_MILLIS) {
//            return "just now";
//        } else if (diff < 2 * MINUTE_MILLIS) {
//            return "a minute ago";
//        } else if (diff < 50 * MINUTE_MILLIS) {
//            return diff / MINUTE_MILLIS + " minutes ago";
//        } else if (diff < 90 * MINUTE_MILLIS) {
//            return "an hour ago";
//        } else if (diff < 24 * HOUR_MILLIS) {
//            return diff / HOUR_MILLIS + " hours ago";
//        } else if (diff < 48 * HOUR_MILLIS) {
//            return "yesterday";
//        } else {
//            return diff / DAY_MILLIS + " days ago";
//        }
//    }

    public static Bitmap blurRenderScript(Context context,Bitmap smallBitmap, int radius) {

        try {
            smallBitmap = RGB565toARGB888(smallBitmap);
        } catch (Exception e) {
            e.printStackTrace();
        }


        Bitmap bitmap = Bitmap.createBitmap(
                smallBitmap.getWidth(), smallBitmap.getHeight(),
                Bitmap.Config.ARGB_8888);

        RenderScript renderScript = RenderScript.create(context);

        Allocation blurInput = Allocation.createFromBitmap(renderScript, smallBitmap);
        Allocation blurOutput = Allocation.createFromBitmap(renderScript, bitmap);

        ScriptIntrinsicBlur blur = ScriptIntrinsicBlur.create(renderScript,
                Element.U8_4(renderScript));
        blur.setInput(blurInput);
        blur.setRadius(radius); // radius must be 0 < r <= 25
        blur.forEach(blurOutput);

        blurOutput.copyTo(bitmap);
        renderScript.destroy();

        return bitmap;

    }

    public static Bitmap RGB565toARGB888(Bitmap img) throws Exception {
        int numPixels = img.getWidth() * img.getHeight();
        int[] pixels = new int[numPixels];

        //Get JPEG pixels.  Each int is the color values for one pixel.
        img.getPixels(pixels, 0, img.getWidth(), 0, 0, img.getWidth(), img.getHeight());

        //Create a Bitmap of the appropriate format.
        Bitmap result = Bitmap.createBitmap(img.getWidth(), img.getHeight(), Bitmap.Config.ARGB_8888);

        //Set RGB pixels.
        result.setPixels(pixels, 0, result.getWidth(), 0, 0, result.getWidth(), result.getHeight());
        return result;
    }


}
